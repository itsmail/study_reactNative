import React, {Component} from 'react';
import {View, Text} from 'react-native';

class LifeCycle extends Component {
    constructor(props){
        super(props)
        console.log('==> lifeCycle Constructor')
        this.state = {
            iklan: 'Helmuth Walter'
        }
    }

    componentDidMount(){
        console.log('==> lifeCycle Did Mount')
        setTimeout(() => {
            this.setState({
                iklan: 'Budi Budeng'
            });
        }, 3000);
    }

    componentDidUpdate(){
        console.log('==> lifeCycle Did Update')
    }

    componentWillUnmount(){
        console.log('lifeCycle Will Unmount')
    }

    render(){
        return(
            <View>
                <Text style={{
                    fontSize: 20, 
                    width: '100%', 
                    height: 25,  
                    textAlign: 'center',
                    backgroundColor: '#feca57',
                    marginVertical: 25
                    }}>Iklan dari {this.state.iklan}</Text>
            </View>
        )
    }
}

export default LifeCycle