import React, {Component, useEffect, useState} from 'react';
import {View, Text, Image} from 'react-native';

// class FlexBox extends Component {
//     constructor(props){
//         super(props)
//         console.log('==> FlexBox Constructor');
//             // untuk merubah jumlah subscriber agar lebih dinamis
//         this.state = {
//             subscriber: 100
//         } 
//     }

//     componentDidMount(){
//         console.log('==> FlexBox Did Mount')
//         setTimeout(() => {
//             this.setState({
//                 subscriber: 400
//             });
//         }, 3000);
        
//     }
//     componentDidUpdate(){
//         console.log('==> FlexBox Did Update')
//     }

//     componentWillUnmount(){
//         console.log('==>FlexBox Will Unmount')
//     }
//     render(){
//         console.log('==> FlexBox Render')
//         return (
//         <View>
//             <View style={{
//                 flexDirection: 'row', 
//                 backgroundColor: '#c8d6e5', 
//                 alignItems: 'center',
//                 justifyContent: 'space-between',
//                 }}>
//                 <Text style={{fontSize: 30}}>Belajar Flex</Text>
//                 <View style={{backgroundColor: '#ee5253', width: 50, height: 50}}/>
//                 <View style={{backgroundColor: '#feca57', width: 50, height: 100}}/>
//                 <View style={{backgroundColor: '#1dd1a1', width: 50, height: 150}}/>
//                 <View style={{backgroundColor: '#5f27cd', width: 50, height: 200}}/>
//             </View>

//             <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
//                 <Text>Beranda</Text>
//                 <Text>Video</Text>
//                 <Text>Playlist</Text>
//                 <Text>Komunitas</Text>
//                 <Text>Channel</Text>
//                 <Text>Tentang</Text>
//             </View>

//             <View style={{marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
//                 <Image source={{uri: 'http://placeimg.com/100/100/people'}} style={{width: 100, height: 100, borderRadius: 50}} />
//                 <View style={{marginLeft: 12}}>
//                     <Text style={{fontSize: 20, fontWeight: 'bold'}}>Channel Youtube</Text>
//                     <Text>{this.state.subscriber} subscriber</Text>
//                 </View>
//             </View>
//         </View>
//         );
//     }
// }


const FlexBox = () => {
    const [subscriber, setSubscriber] = useState(200)
    // untuk menghandle component did mount, did update, will unmount
    // nama lain dari function ini adalah hooks dan lebih efective dari class compontent
    useEffect(() => {
        console.log('did mount');
        setTimeout(()=>{
            setSubscriber(400);
        },2000)
        return(
            console.log('did update')
        )
    }, [subscriber]);

    return (
        <View>
            <View style={{
                flexDirection: 'row', 
                backgroundColor: '#c8d6e5', 
                alignItems: 'center',
                justifyContent: 'space-between',
                }}>
                <Text style={{fontSize: 30}}>Belajar Flex</Text>
                <View style={{backgroundColor: '#ee5253', width: 50, height: 50}}/>
                <View style={{backgroundColor: '#feca57', width: 50, height: 100}}/>
                <View style={{backgroundColor: '#1dd1a1', width: 50, height: 150}}/>
                <View style={{backgroundColor: '#5f27cd', width: 50, height: 200}}/>
            </View>

            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <Text>Beranda</Text>
                <Text>Video</Text>
                <Text>Playlist</Text>
                <Text>Komunitas</Text>
                <Text>Channel</Text>
                <Text>Tentang</Text>
            </View>

            <View style={{marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                <Image source={{uri: 'http://placeimg.com/100/100/people'}} style={{width: 100, height: 100, borderRadius: 50}} />
                <View style={{marginLeft: 12}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>Channel Youtube</Text>
                    <Text>{subscriber} subscriber</Text>
                </View>
            </View>
        </View>
        );
}
export default FlexBox;