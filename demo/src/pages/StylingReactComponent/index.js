import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import macbook from '../../assets/image/macbook.jpg'

const StylingReactComponent = () => {
    return (
        <View style={{alignSelf: 'center'}}>
            <Text style={styles.text}>Styling Component</Text>
            <View style={{
                height: 100,
                backgroundColor: '#0abde3',
                borderColor: '#5f27cd',
                marginTop: 20,
            }}>
              <Text style={{padding: 30, fontSize: 20}}>Belajar Styling Component</Text>
            </View>
  
            <View style = {{
                padding: 12, 
                backgroundColor:'#F2F2F2', 
                width: 300, 
                alignItems: 'center' 
                }}>
                <Image source={macbook} style={{
                    width: 188, 
                    height: 107
                    }} />
                <Text style = {{
                    marginVertical: 10, 
                    textAlign: 'center'
                    }}>Macbook Air Pro 2022</Text>
                <Text style={{textAlign: 'center'}}>Magelang, USA</Text>
                <View style={{padding: 12, 
                    margin: 12, 
                    backgroundColor: '#25cf43', 
                    width: 180, 
                    borderRadius: 50
                    }}>
                  <Text style={{textAlign: 'center'}}>Beli Product</Text>
                </View>
            </View>
        </View>
    )
  }
  
  const styles = StyleSheet.create({
    text: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#10ac84',
        marginLeft: 20,
        marginTop: 40,
        textAlign: 'center'
    }
  })

export default StylingReactComponent;