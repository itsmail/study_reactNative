import React, {Component} from 'react'
import {View, Text, TextInput, Image} from 'react-native'

const SampleComponent = () => {
    return (
      <View style={{backgroundColor:'orange'}}>
        <Text style={{fontSize: 25, fontWeight: 'bold', textAlign: 'center'}}>Belajar Basic Component</Text>
        <ListKawan />
        <Photo />
        <TextInput style={{borderWidth:1, backgroundColor: 'white', color:'black'}}/>
        <GreenBox />
        <Profile />
        
      </View>
    )
}
const ListKawan = () => {
  return (
    <View>
      <Text>List Teman</Text>
      <Text>~Faishal</Text>
      <Text>~Fadhil</Text>
      <Text>~Wiral</Text>
      <Text>~Komarudin</Text>
      <Text>==================================</Text>
    </View>
  )
}

const Photo = () => {
    return (
      <View>
        <Image source={{uri:'https://placeimg.com/100/100/nature'}} 
          style={{
            width:100, 
            height:100, 
            alignSelf:'center', 
            borderWidth: 8, 
            borderColor: 'brown'
          }}/>
        <Text style={{textAlign:'center', marginVertical: 6, fontSize: 19}}>Gambar apa yang kamu lihat?</Text>

      </View>        
    )}

class GreenBox extends Component{
  render(){
      return (
        <Text style={{
          fontSize: 19,
          textAlign: 'center',
          fontStyle: 'italic',
        }}>Siapa gambar yang ada di bawah ini?</Text>
      )
    }
}

class Profile extends Component{
  render(){
    return (
      <View>
        <Image source={{uri:'http://placeimg.com/100/100/people'}} style={{
          width: 100, 
          height: 100, 
          borderRadius: 50,
          alignSelf: 'center'
          }} />
        <TextInput style={{borderWidth:1, backgroundColor:'white'}}/>
      </View>
    )
  }
}

export default SampleComponent;